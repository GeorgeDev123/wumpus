package game.Wumpus.Enum;

public enum GameItemEnum {
    GAME_ITEM, GOLD, PIT, WUMPUS, CLEAR_GROUND
}
