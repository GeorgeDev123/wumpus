package game.Wumpus.GameItem;

import game.Wumpus.Enum.GameItemEnum;

public class Gold extends GameItem {
    public Gold() {
        super('g', GameItemEnum.GOLD);
    }
}
