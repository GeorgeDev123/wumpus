package game.Wumpus.GameItem;

import game.Wumpus.Enum.GameItemEnum;

public class Wumpus extends GameItem {
    public Wumpus() {
        super('w', GameItemEnum.WUMPUS);
    }
}
