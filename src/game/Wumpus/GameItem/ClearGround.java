package game.Wumpus.GameItem;

import game.Wumpus.Enum.GameItemEnum;

public class ClearGround extends GameItem {
    public ClearGround() {
        super('.', GameItemEnum.CLEAR_GROUND);
    }
}
