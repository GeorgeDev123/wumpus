package game.Wumpus.GameItem;

import game.Wumpus.Enum.GameItemEnum;

public class GameItem {
    protected char charDisplay;
    public GameItemEnum ItemEnum = GameItemEnum.GAME_ITEM;
    public GameItem(char c, GameItemEnum itemEnum){
        this.charDisplay = c;
        this.ItemEnum = itemEnum;
    }
    public char display(){
        return charDisplay;
    }
}
