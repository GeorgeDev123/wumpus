package game.Wumpus.GameItem;

import game.Wumpus.Enum.GameItemEnum;

public class Pit extends GameItem {
    public Pit() {
        super('p', GameItemEnum.PIT);
    }
}
