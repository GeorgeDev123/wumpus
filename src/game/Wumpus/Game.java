package game.Wumpus;

import game.Wumpus.Enum.GameItemEnum;
import game.Wumpus.GameItem.*;

import java.util.Random;
import java.util.Scanner;


public class Game {
    private final int boardXSize = 4;
    private final int boardYSize = 4;
    private final char playerSymbol = '*';
    private Random randomGenerator = new Random();
    public GameItem[][] board = new GameItem[boardXSize][boardYSize];
    Scanner scanner = new Scanner(System.in);
    private static int[] playerPosition;

    //Init the board item with all the clear ground items
    private void initBoard() {
        //Set all to clear groud first.
        for (int i = 0; i < boardXSize; i++) {
            for (int y = 0; y < boardYSize; y++) {
                board[i][y] = new ClearGround();
            }
        }
    }
    /**
    *Meeting requirement
        * a private method setBoard(), which instantiates objects on the board
     */
    public void setBoard() {
        initBoard();
        int guess;

        //Exactly one Wumpus
        int[] wumpusAxis = getRandomGameItemAxis();
        board[wumpusAxis[0]][wumpusAxis[1]] = new Wumpus();

        //Exactly three pits
        for(int i=0; i<3; i++){
            int[] pitAxis = getRandomGameItemAxis();
            board[pitAxis[0]][pitAxis[1]] = new Pit();
        }

        //At leas one Gold
        int[] goldAxis = getRandomGameItemAxis();
        board[goldAxis[0]][goldAxis[1]] = new Gold();
        int goldCounter = 1;

        //rest of the Golds
        for (int i = 0; i < boardXSize; i++) {
            for (int y = 0; y < boardYSize; y++) {
                if (board[i][y].ItemEnum == GameItemEnum.GOLD) {
                    guess = randomGenerator.nextInt(4);
                    if (guess == 0 && goldCounter < 3) {
                        goldCounter++;
                        board[i][y] = new Gold();
                    }
                }
            }
        }
        //Initiate player position at a random clear ground position
        playerPosition = getRandomGameItemAxis();
    }

    //Get a random Clear Ground item from the board
    private int[] getRandomGameItemAxis(){
        while(true){
            int randomX = randomGenerator.nextInt(4);
            int randomY = randomGenerator.nextInt(4);
            if(board[randomX][randomY].ItemEnum == GameItemEnum.CLEAR_GROUND){
                int[] xyAxis = new int[2];
                xyAxis[0] = randomX;
                xyAxis[1] = randomY;
                return xyAxis;
            }
        }
    }
    /**
    *Meeting requirement:
        * a private method display() which will display the board.
     */
    private void display() {
        for (int i = 0; i < boardXSize; i++) {
            for (int y = 0; y < boardYSize; y++) {
                if (i == playerPosition[0] && y == playerPosition[1]) {
                    System.out.print(playerSymbol);
                } else {
                    System.out.print(board[i][y].display());
                }
            }
            System.out.println(System.lineSeparator());
        }
    }
    /**
    *Meeting requirement:
        *a private method senseNearby(), which displays text about what the player can sense from the board elements
        *immediately surrounding the player.
     */
    private void senseNearby() {
        int xAxis = playerPosition[0];
        int yAxis = playerPosition[1];

        int[] upSenseAxis = new int[2];
        int[] downSenseAxis = new int[2];
        int[] leftSenseAxis = new int[2];
        int[] rightSenseAxis = new int[2];

        //Up direction
        upSenseAxis[0] = adjustAxis(xAxis, true);
        upSenseAxis[1] = yAxis;

        //Down direction
        downSenseAxis[0] = adjustAxis(xAxis,false);
        downSenseAxis[1] = yAxis;

        //Left direction
        leftSenseAxis[1] = adjustAxis(yAxis, true);
        leftSenseAxis[0] = xAxis;

        //Right direction
        rightSenseAxis[1] = adjustAxis(yAxis,false);
        rightSenseAxis[0] = xAxis;

        //Display the text
        String senseText = senseText(board[upSenseAxis[0]][upSenseAxis[1]]) + " ";
        senseText += senseText(board[downSenseAxis[0]][downSenseAxis[1]]) + " ";
        senseText += senseText(board[leftSenseAxis[0]][leftSenseAxis[1]]) + " ";
        senseText += senseText(board[rightSenseAxis[0]][rightSenseAxis[1]]) + " ";
        System.out.println(senseText);

    }
    //Algorithm for adjusting the nearby positions
    private int adjustAxis(int axis, boolean isUpOrLeftDirection) {
        if (isUpOrLeftDirection) {
            if (axis == 0) {
                return boardXSize - 1;
            } else {
                return axis - 1;
            }
        }
        else{
            if (axis == boardXSize - 1) {
                return 0;
            } else {
                return axis + 1;
            }
        }
    }
    /**
     * Return text for sense text messages based on the displayChar
     */
    private String senseText(GameItem gameItem) {

        switch (gameItem.ItemEnum) {
            case WUMPUS:
                return "There is a Wumpus around";
            case PIT:
                return "There is a Pit around";
            case CLEAR_GROUND:
                return "There is a Clear Ground around";
            case GOLD:
                return "There is a Gold around";
            default:
                return "";
        }
    }
    /**
    *Meeting requirement
        * a private method menu() which will provide a menu asking the user to make a choice from the following and obtain the user input:
        *=====Wumpus====
        *1. Move player left
        *2. Move player right
        *3. Move player up
        *4. Move player down
        *5 Quit
     */
    private void menu(){
        System.out.println("=====Wumpus=====");
        System.out.println("1. Move player left");
        System.out.println("2. Move player right");
        System.out.println("3. Move player up");
        System.out.println("4. Move player down");
        System.out.println("5. Quit");

        boolean validInput = false;
        do {
            try {
                int choice = Integer.parseInt(scanner.next());
                if (choice < 1 || choice > 5) {
                    throw new Exception("Bad entry");
                }
                validInput = true;
                switch (choice) {
                    case 1:
                        playerPosition[1] = adjustAxis(playerPosition[1],true);
                        break;
                    case 2:
                        playerPosition[1] = adjustAxis(playerPosition[1],false);
                        break;
                    case 3:
                        playerPosition[0] = adjustAxis(playerPosition[0],true);
                        break;
                    case 4:
                        playerPosition[0] = adjustAxis(playerPosition[0],false);
                        break;
                    case 5:
                        System.exit(0);
                    default:
                        break;
                }
                moveAndScore();
            }
            catch (Exception ex){
                System.out.println("Invalid input, pls try again");
            }
        }
        while(!validInput);
    }
    /**
    *Meeting requirement:
        *If the player moves onto a pit or the Wumpus, the player dies and the game ends.
        *If the player moves onto gold, the game score increases by one and the gold is replaced by ClearGround.
     */
    private void moveAndScore(){
        GameItem currentItem = board[playerPosition[0]][playerPosition[1]];
        if(currentItem.ItemEnum == GameItemEnum.PIT || currentItem.ItemEnum == GameItemEnum.WUMPUS){
            System.out.println("You had moved onto a pit or Wumpus, GG");
            System.exit(0);
        }
        else if (currentItem.ItemEnum == GameItemEnum.GOLD){
            System.out.println("You had moved onto a gold, you scored");
            board[playerPosition[0]][playerPosition[1]] = new ClearGround();
        }
    }
    /**
    *Meeting requirement:
        *a public method runGame() that will display the board, print out what
        *the player can sense, present the menu and process the user's decision
        *according to the game play described above.
     */
    public void runGame(){
        setBoard();
        while(true) {
            display();
            senseNearby();
            menu();
        }
    }
}
