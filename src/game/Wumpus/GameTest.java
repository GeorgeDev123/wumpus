package game.Wumpus;

import game.Wumpus.GameItem.GameItem;
import game.Wumpus.GameItem.Gold;
import game.Wumpus.GameItem.Pit;
import game.Wumpus.GameItem.Wumpus;

import org.junit.Assert;

public class GameTest {
    @org.junit.Test
    public void setBoard() throws Exception {
        for (int x =0 ;x <10; x++) {
            Game newGame = new Game();
            newGame.setBoard();
            int counterWumpus = 0;
            int counterGold = 0;
            int counterPits = 0;

            for (GameItem[] items : newGame.board
                    ) {
                for (GameItem item : items) {
                    if (item.getClass().equals(Wumpus.class)) {
                        counterWumpus++;
                    } else if (item.getClass().equals(Gold.class)) {
                        counterGold++;
                    } else if (item.getClass().equals(Pit.class)) {
                        counterPits++;
                    }
                }
            }
            Assert.assertEquals(counterWumpus, 1);
            Assert.assertEquals(counterPits, 3);
            Assert.assertTrue(counterGold >= 1 && counterGold <= 3);
        }
    }

}